﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FibonnaciRest.Model
{
    public static class Fibonacci
    {
        public static List<ulong> getFibonacciList(int number)
        {
            List<ulong> rezult = new List<ulong>();

            if (number < 0)
                throw new FormatException("Negative numbers are not allowed");

            if (number >= 0)
                rezult.Add(0);

            if (number >= 1)
                rezult.Add(1);

            for (int i = 2; i < number; i++)               
                rezult.Add(checked(rezult[i - 1] + rezult[i - 2])); //check ulong overflow

            return rezult;
        }
    }
}
