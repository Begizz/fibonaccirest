﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FibonnaciRest.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FibonnaciRest.Controllers
{
    [Route("api/fibonacci")]
    [ApiController]
    public class FibonaciiController : ControllerBase
    {

        private readonly ILogger<FibonaciiController> _logger;

        public FibonaciiController(ILogger<FibonaciiController> logger)
        {
            _logger = logger;
        }

        // GET api/fibonacci/5
        /// <summary>
        /// For generating Fibonnaci numbers
        /// </summary>
        /// <param name="value"></param>
        /// <returns>List of Fibonacci numbers</returns>
        [HttpGet("{value}")]
        [ProducesResponseType(200)] //OK
        [ProducesResponseType(400)] //BadRequest
        [ProducesResponseType(500)] //InternalServerError
        
        public ActionResult Get(int value)
        {
            try
            {
                return Ok(Fibonacci.getFibonacciList(value));
            }
            catch (OverflowException ex)
            {
                return BadRequest(new { value = ex.Message });
            }
            catch (FormatException ex)
            {
                return BadRequest(new { value = ex.Message });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
