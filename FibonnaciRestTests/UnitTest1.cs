using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace FibonnaciRestTests
{
    [TestClass]
    public class FibonnaciTest
    {

        private const long Expected = 610;

        [TestMethod]
        public void getFibonacciListTest()
        {
            var result = FibonnaciRest.Model.Fibonacci.getFibonacciList(16);
            Assert.AreEqual(result.Last(), Expected);
        }
    }
}
